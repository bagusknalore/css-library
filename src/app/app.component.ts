import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  disableChecked = true;
  title = 'prototype-material';
  neutral = false;
  labelPosition: 'before' | 'after' = 'after';
}
